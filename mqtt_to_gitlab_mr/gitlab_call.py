# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
from dataclasses import dataclass

from gitlab.client import Gitlab
from loguru import logger
import requests

@dataclass
class GitlabCall:  # pylint: disable=too-many-instance-attributes
    # For now, we'll keep all these attributes
    # needs to be refactored one day
    pipeline_id: int
    target_project_id: int
    merge_request_id: int
    last_commit_id: str
    private_token: str
    external_status_check_id: int
    host: str = "https://gitlab.com"

    def __post_init__(self) -> None:
        gitlab_client = Gitlab(self.host, private_token=self.private_token)
        self.target_project = gitlab_client.projects.get(
            self.target_project_id,
            retry_transient_errors=True,
        )
        self.merge_request = self.target_project.mergerequests.get(
            self.merge_request_id,
            retry_transient_errors=True,
        )

    def comment(self, message: str) -> None:
        self.merge_request.notes.create({'body': message})

    def approve_pipeline(self) -> None:
        headers = {'PRIVATE-TOKEN': self.private_token}
        params = {
            'sha': self.last_commit_id,
            'external_status_check_id': self.external_status_check_id,
        }
        base_url = "{}/api/v4".format(self.host)
        project_url = "{}/projects/{}".format(base_url, self.target_project_id)
        url = "{}/merge_requests/{}/status_check_responses".format(
            project_url,
            self.merge_request_id,
        )
        logger.debug("url to send: {}", url)
        logger.debug("params used: {}", params)
        request = requests.post(url, params=params, headers=headers)
        logger.debug("gitlab return: {}", request.text)
