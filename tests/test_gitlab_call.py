# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock

from mqtt_to_gitlab_mr.gitlab_call import GitlabCall

class RequestMock():  # pylint: disable=too-few-public-methods
    def __init__(self, text: str) -> None:
        self.text = text

class MergeRequestMock:  # pylint: disable=too-few-public-methods
    def __init__(self) -> None:
        self.notes = mock.Mock()


class MergeRequestsMock:  # pylint: disable=too-few-public-methods
    def __init__(self) -> None:
        self.mergerequest = MergeRequestMock()

    def get(self, _id, retry_transient_errors=True):  # pylint: disable=unused-argument
        return self.mergerequest

class ProjectMock:  # pylint: disable=too-few-public-methods
    def __init__(self, source=False) -> None:
        self.mergerequests = MergeRequestsMock()
        self.name = "target project"
        if source:
            self.name = "source project"


class ProjectsMock:  # pylint: disable=too-few-public-methods
    def __init__(self) -> None:
        self.target_project = ProjectMock()

    def get(self, id, retry_transient_errors=True): # pylint: disable=redefined-builtin,invalid-name,unused-argument
        return self.target_project

class GitlabMock:  # pylint: disable=too-few-public-methods
    def __init__(self) -> None:
        self.projects = ProjectsMock()

@mock.patch('mqtt_to_gitlab_mr.gitlab_call.Gitlab')
def test_comment(mock_gitlab):
    mock_gitlab.return_value = GitlabMock()
    gitlab_call = GitlabCall(1234, 5678, 9012, "abc", "token", 12)
    gitlab_call.comment('comment')
    expected_calls = [mock.call.create({'body': 'comment'})]
    assert gitlab_call.merge_request.notes.method_calls == expected_calls


@mock.patch('mqtt_to_gitlab_mr.gitlab_call.Gitlab')
@mock.patch("requests.post")
def test_approve_pipeline(mock_requests, mock_gitlab):
    mock_requests.return_value = RequestMock("text")
    mock_gitlab.return_value = GitlabMock()
    gitlab_call = GitlabCall(1234, 5678, 9012, "abc", "token", 12)
    gitlab_call.approve_pipeline()
    base_url = "https://gitlab.com/api/v4/projects/5678"
    expected_url = "{}/merge_requests/9012/status_check_responses".format(base_url)
    expected_headers = {'PRIVATE-TOKEN': 'token'}
    expected_params = {
        'sha': 'abc',
        'external_status_check_id': 12,
    }
    mock_requests.assert_called_once_with(
        expected_url,
        params=expected_params,
        headers=expected_headers,
    )
