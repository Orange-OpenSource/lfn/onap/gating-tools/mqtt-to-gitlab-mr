# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import os

import mock

from  mqtt_to_gitlab_mr import main

@mock.patch.dict(os.environ, {"CONFIG_FILE": "etc/gating-launcher.conf", 'PRIVATE_TOKEN': 'token'})
@mock.patch('mqtt_to_gitlab_mr.main._create_mqtt_client')
def test_start(create_mqtt_client_mock, valid_userdata, valid_config_parsed):
    mqtt_client_mock = mock.MagicMock()
    create_mqtt_client_mock.return_value = mqtt_client_mock
    expected_userdata = valid_userdata
    main.start()
    create_mqtt_client_mock.assert_called_once_with(
        valid_config_parsed,
        'mqtt.client.subscribe',
        main.on_connect_subscribe,
        on_message_callback=main.on_message,
        userdata=expected_userdata,
    )
    mqtt_client_mock.loop_forever.assert_called_once()
