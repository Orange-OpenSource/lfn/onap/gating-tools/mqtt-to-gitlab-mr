# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import json

import mock

from paho.mqtt.client import MQTTMessage

from mqtt_to_gitlab_mr import main


@mock.patch('mqtt_to_gitlab_mr.main.GitlabCall')
def test_OK(  # pylint: disable=invalid-name
    gitlab_call_mock,
    valid_msg,
    valid_userdata,
):
    gitlab_call_instance_mock = mock.MagicMock()
    gitlab_call_mock.return_value = gitlab_call_instance_mock
    main.on_message(None, valid_userdata, valid_msg)
    gitlab_call_mock.assert_called_once_with(
        416433035,
        30958181,
        3,
        'beb2ea9bc37156c6f616fc2a22d7731d007c4586',
        'token',
        7890,
        host='https://gitlab.example.com',
    )
    comment_call = mock.call().comment('Deployment put on queue, number 1 on the queue.')
    gitlab_call_mock.assert_has_calls([comment_call])

@mock.patch('mqtt_to_gitlab_mr.main.GitlabCall')
def test_OK_approve(  # pylint: disable=invalid-name
    gitlab_call_mock,
    valid_msg,
    valid_userdata,
):
    msg_raw = json.loads(valid_msg.payload)
    msg_raw['score'] = 1
    msg = MQTTMessage()
    msg.payload = json.dumps(msg_raw)
    gitlab_call_instance_mock = mock.MagicMock()
    gitlab_call_mock.return_value = gitlab_call_instance_mock
    main.on_message(None, valid_userdata, msg)
    gitlab_call_mock.assert_called_once_with(
        416433035,
        30958181,
        3,
        'beb2ea9bc37156c6f616fc2a22d7731d007c4586',
        'token',
        7890,
        host='https://gitlab.example.com',
    )
    comment_call = mock.call().comment('Deployment put on queue, number 1 on the queue.')
    approve_call = mock.call().approve_pipeline()
    gitlab_call_mock.assert_has_calls([comment_call, approve_call])

@mock.patch('mqtt_to_gitlab_mr.main.GitlabCall')
def test_no_msg_JSON(   # pylint: disable=invalid-name
    gitlab_call_mock,
    valid_userdata,
    caplog,
):
    msg = MQTTMessage()
    msg.payload = 'test'
    main.on_message(None, valid_userdata, msg)
    gitlab_call_mock.assert_not_called()
    assert "This is not a valid JSON payload" in caplog.text

@mock.patch('mqtt_to_gitlab_mr.main.GitlabCall')
def test_bad_msg(
    gitlab_call_mock,
    valid_msg,
    valid_userdata,
    caplog
):
    msg_raw = json.loads(valid_msg.payload)
    msg_raw.pop('gerrit_patchset')
    msg = MQTTMessage()
    msg.payload = json.dumps(msg_raw)
    main.on_message(None, valid_userdata, msg)
    gitlab_call_mock.assert_not_called()
    assert  "the key 'gerrit_patchset' wasn't found in the dict" in caplog.text

@mock.patch('mqtt_to_gitlab_mr.main.GitlabCall')
def test_bad_userdata(
    gitlab_call_mock,
    valid_msg,
    valid_userdata,
    caplog
):
    userdata = valid_userdata
    userdata.pop('external_status_check_id')
    main.on_message(None, userdata, valid_msg)
    gitlab_call_mock.assert_not_called()
    assert "the key 'external_status_check_id' wasn't found in the dict" in caplog.text
