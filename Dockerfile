FROM nexus3.onap.org:10001/onap/integration-python:9.1.0

LABEL name="mqtt_to_gitlab_mr" \
      version="v0.1.dev0" \
      architecture="x86_64"

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install --no-cache-dir -r requirements.txt

COPY etc/gating-launcher.conf /app/etc/
COPY mqtt_to_gitlab_mr  /app/mqtt_to_gitlab_mr/

ENV PYTHONPATH="${PYTHONPATH}:/app"
ENV CONFIG_FILE=/app/etc/mqtt-to-gitlab-mr.conf

CMD [ "python3", "mqtt_to_gitlab_mr/main.py"]
